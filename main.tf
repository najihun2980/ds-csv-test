terraform {
  required_providers {
    tfe = {
      source = "hashicorp/tfe"
      version = "0.44.1"
    }
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "15.11.0"
    }
    null = {
      source = "hashicorp/null"
      version = "3.2.1"
    }
  }
}
provider "tfe" {
  token   = var.token
}
provider "gitlab" {
  token = var.gitlab_token
}
//----------------------------------------------------------------------------------------------

##csv 파일을 읽어옴
locals {
  csv_data = file("./template.csv")
  csv_de_data = csvdecode(local.csv_data)
}

##gitlab repo 복제 및 실행
resource "gitlab_project" "fork" {
  for_each    = { for ID in local.csv_de_data : ID.index => ID if ID.enable == "true" }
  name        = "aws_instance-${each.value.index}"
  description = "This is a fork-${each.value.index}"
  #  forked_from_project_id = 46065486
  import_url = "https://gitlab.com/gweowe/terraform_instance_repo.git"
  #import_url             = "https://gitlab.com/cmonworld1227/terraform-aws-vm-${each.value.ID}"
  #mirror                 = true
}


##worksapce 생성
resource "tfe_workspace" "tfe_ws" {
  for_each     = { for ID in local.csv_de_data : ID.index => ID if ID.enable == "true" }
  name         = "aws_instance-${each.value.index}"
  organization = "dwwon-samsung-poc"
  tag_names    = ["aws", "dwwon"]
  allow_destroy_plan = true
  auto_apply         = true
  //queue_all_runs = false
  vcs_repo {
    identifier = "gweowe/${gitlab_project.fork[each.key].name}"
    oauth_token_id = "ot-9zuuwhXFtW8DWuKW"
  }
}

#csv가 담긴 워크스페이스를 가져옴
data "tfe_workspace" "csv_module" {
  name         = "tfe_csv_module"
  organization = "dwwon-samsung-poc"
}
//----------------------------------------------------------------------------------------------
## vm생성에 필요한 config를 입력하여 workspace에 추가
resource "tfe_variable" "index" {
  for_each        = { for ID in local.csv_de_data : ID.index => ID if ID.enable == "true" }
  key             = "index"
  value           = each.value.index
  category        = "terraform"
  description     = "the number of index"
  workspace_id    = tfe_workspace.tfe_ws[each.key].id
  depends_on      = [tfe_workspace.tfe_ws.each.key]
}

resource "tfe_variable" "CSP" {
  for_each        = { for ID in local.csv_de_data : ID.index => ID if ID.enable == "true" }
  key             = "CSP"
  value           = each.value.CSP
  category        = "terraform"
  description     = "type of CSP"
  workspace_id    = tfe_workspace.tfe_ws[each.key].id
  depends_on      = [tfe_workspace.tfe_ws.each.key]
}

resource "tfe_variable" "Type" {
  for_each        = { for ID in local.csv_de_data : ID.index => ID if ID.enable == "true" }
  key             = "Type"
  value           = each.value.Type
  category        = "terraform"
  description     = "type of instance"
  workspace_id    = tfe_workspace.tfe_ws[each.key].id
  depends_on      = [tfe_workspace.tfe_ws.each.key]
}

resource "tfe_variable" "Description" {
  for_each        = { for ID in local.csv_de_data : ID.index => ID if ID.enable == "true" }
  key             = "Description"
  value           = each.value.Description
  category        = "terraform"
  description     = "Description of csv"
  workspace_id    = tfe_workspace.tfe_ws[each.key].id
  depends_on      = [tfe_workspace.tfe_ws.each.key]
}

resource "tfe_variable" "Num" {
  for_each        = { for ID in local.csv_de_data : ID.index => ID if ID.enable == "true" }
  key             = "Num"
  value           = each.value.Num
  category        = "terraform"
  description     = "the number of resources"
  workspace_id    = tfe_workspace.tfe_ws[each.key].id
  depends_on      = [tfe_workspace.tfe_ws.each.key]
}

resource "tfe_variable" "UserID" {
  for_each        = { for ID in local.csv_de_data : ID.index => ID if ID.enable == "true" }
  key             = "UserID"
  value           = each.value.UserID
  category        = "terraform"
  description     = "applicant's User ID"
  workspace_id    = tfe_workspace.tfe_ws[each.key].id
  depends_on      = [tfe_workspace.tfe_ws.each.key]
}

resource "tfe_variable" "ami" {
  for_each        = { for ID in local.csv_de_data : ID.index => ID if ID.enable == "true" }
  key             = "ami"
  value           = each.value.ami
  category        = "terraform"
  description     = "aws ami"
  workspace_id    = tfe_workspace.tfe_ws[each.key].id
  depends_on      = [tfe_workspace.tfe_ws.each.key]
}

resource "tfe_variable" "enable" {
  for_each        = { for ID in local.csv_de_data : ID.index => ID if ID.enable == "true" }
  key             = "enable"
  value           = each.value.enable
  category        = "terraform"
  description     = "live status"
  workspace_id    = tfe_workspace.tfe_ws[each.key].id
  depends_on      = [tfe_workspace.tfe_ws.each.key]
}

## 공동 관리중인 aws key를 불러옴
data "tfe_variable_set" "aws_credential" {
  name         = "aws_credential"
  organization = "dwwon-samsung-poc"
}

//----------------------------------------------------------------------------------------------
//runtrigger
resource "tfe_run_trigger" "csv_updated" {
  for_each = { for ID in local.csv_de_data : ID.index => ID if ID.enable == "true" }

  workspace_id  = tfe_workspace.tfe_ws[each.key].id
  sourceable_id = data.tfe_workspace.csv_module.id
}

/*
## instance workspace destroy
resource "null_resource" "destroy" {
  for_each = { for ID in local.csv_de_data : ID.index => ID if ID.enable == "false" }

  provisioner "local-exec" {
    command = <<-EOT
      curl --request POST --header "Authorization: Bearer ${var.token}" --header "Content-Type: application/vnd.api+json" --data '{
  "data": {
    "attributes": {
      "is-destroy": "true",
      "auto-apply": "true"
    },
    "type": "runs",
    "relationships": {
      "workspace": {
        "data": {
          "type": "workspaces",
          "id": "${tfe_workspace.tfe_ws[each.key].id}"
        }
      }
    }
  }
}' https://app.terraform.io/api/v2/runs
    EOT
   }
}
*/

/*
## destroy workspace에서 remote하기 위해 output 생성
output "csv" {
  value = local.csv_de_data
}
*/
